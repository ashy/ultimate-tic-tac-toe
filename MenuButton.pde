public class MenuButton extends Button {
    public color fillColor = Colors.blue;
    public color textColor = Colors.white;
    public color strokeColor = Colors.white;
    public float strokeWeight = 0.0f;
    
    public color hoverFillColor = Colors.blue;
    public color hoverTextColor = Colors.white;
    public color hoverStrokeColor = Colors.white;
    public float hoverStrokeWeight = 8.0f;
    
    public PFont font = kalamRegular32;
    public float btnTextSize = 32f;
    public float radius = 6f;
    
    public String label;

    public MenuButton(Runnable onClick, String label) {
        super();
        this.onClick = onClick;
        this.label = label;
    }
    public MenuButton(Runnable onClick, String label, float x, float y, float w, float h) {
        super();
        this.onClick = onClick;
        this.label = label;
        
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    @Override
    public void draw() {
        super.draw();
        
        if(isHovered()) {
            fill(hoverFillColor);
            stroke(hoverStrokeColor);
            strokeWeight(hoverStrokeWeight);
        } else {
            fill(fillColor);
            stroke(strokeColor);
            strokeWeight(strokeWeight);
        }
        rect(x, y, w, h, radius);
        
        if(isHovered()) {
            fill(hoverTextColor);
        } else {
            fill(textColor);
        }
        textFont(font);
        textSize(btnTextSize);
        textAlign(CENTER, CENTER);
        text(label, x + w/2f, y + h/2f);
    }
}
