import java.util.List;

public class MenuScreen extends Screen {
    public final float btnInset = 120f;
    public final float btnPadding = 16f;
    public final float btnRowMinWidth = 480f; // The buttons will try not to get smaller than this, until padding is too small.
    public final float btnMinWidth = 220f; // Buttons on the same row will split up if they get smaller than this
    public final float btnHeight = 48f;
    
    public float yOffset; // Start buttons from height-yOffset
    public float buttonStart;
    
    public List<List<MenuButton>> buttons;
    
    public MenuScreen(float yOffset) {
        this.yOffset = yOffset;
        this.buttonStart = height-yOffset;
    }
    public MenuScreen() {
        this(16f);
    }

    @Override
    public void setup() {
        this.buttons = new ArrayList<List<MenuButton>>();
    }
    
    @Override
    public void draw() {
        this.buttonStart = height-yOffset;
        this.updateButtonPositions();
    }
    
    public void drawButtons() {
        for(List<MenuButton> l : this.buttons) {
            for(MenuButton b : l) {
                b.draw();
            }
        }
    }
    
    protected void updateButtonPositions() {
        float y = buttonStart - btnHeight;
        for(int i = this.buttons.size()-1; i >= 0; i--) {
            List<MenuButton> row = this.buttons.get(i);
            
            float inset = width-btnInset*2 > btnRowMinWidth ? btnInset : max((width-btnRowMinWidth)/2f, btnPadding);
            float rowWidth = width-inset*2;
            
            float x = inset;
            float w = (rowWidth - btnPadding*(row.size()-1)) / row.size();
            
            if(row.size() > 1 && w < btnMinWidth) { // Split up buttons if too small
                for(int j = row.size()-1; j >= 0; j--) {
                    MenuButton button = row.get(j);
                    
                    button.x = x;
                    button.y = y;
                    
                    button.h = btnHeight;
                    button.w = rowWidth;
                    
                    y -= btnHeight + btnPadding;
                }
            } else {
                for(int j = 0; j < row.size(); j++) {
                    MenuButton button = row.get(j);
                    
                    button.x = x;
                    button.y = y;
                    
                    button.h = btnHeight;
                    button.w = w;
                    
                    x += button.w + btnPadding;             
                }
                y -= btnHeight + btnPadding;
            }
        }
    }
}
