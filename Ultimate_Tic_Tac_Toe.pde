public PFont kalamRegular32;
public PFont kalamBold48;
public PFont kalamBold96;
public PFont kalamBold255;

public final boolean debug = true;

private boolean lastMousePressed;
private boolean lastKeyPressed;

void setup() {
    if(displayDensity() > 1) { // Load double-res fonts on hidpi displays
        kalamRegular32 = loadFont("Kalam-Regular-64.vlw");
        kalamBold48 = loadFont("Kalam-Bold-96.vlw");
        kalamBold96 = loadFont("Kalam-Bold-192.vlw");
        kalamBold255 = loadFont("Kalam-Bold-255.vlw");
        
        println("displayDensity() > 1, using double res fonts.");
    } else {
        kalamRegular32 = loadFont("Kalam-Regular-32.vlw");
        kalamBold48 = loadFont("Kalam-Bold-48.vlw");
        kalamBold96 = loadFont("Kalam-Bold-96.vlw");
        kalamBold255 = loadFont("Kalam-Bold-255.vlw");
    }
    
    surface.setTitle("Ultimate Tic Tac Toe");
    surface.setResizable(true);
    size(854, 480, P2D);
    pixelDensity(displayDensity());
    smooth(4);
    
    switchScreen(new TitleScreen());
}

void draw() {
    currentScreen.draw();
    
    lastMousePressed = mousePressed;
    lastKeyPressed = keyPressed;
}

void keyPressed() {
    currentScreen.keyPressed();
    if(key == ESC) {
        key = 0; // Don't quit on pressing escape
    }
}
