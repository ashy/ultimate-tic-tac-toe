import java.util.List;

public class TitleScreen extends MenuScreen {
    public final String titleText = "Ultimate\nTic-Tac-Toe";
    public final float titleTextSize = 96f;
    public final float titlePaddingX = 16f;
    public final float titlePaddingY = 0f;

    @Override
    public void setup() {
        super.setup();
        this.buttons.add(List.of(new MenuButton[] {
            new MenuButton(() -> {
                switchScreen(new GameScreen());
            }, "Play")
        }));
        this.buttons.add(List.of(new MenuButton[] {
            new MenuButton(() -> {
                switchScreen(new OptionsScreen());
            }, "Options")
        }));
        this.buttons.add(List.of(new MenuButton[] {
            new MenuButton(() -> {
                switchScreen(new AboutScreen());
            }, "About"),
            new MenuButton(() -> {
                exit();
            }, "Quit")
        }));
    }
    
    @Override
    public void draw() {
        super.draw();
        
        background(Colors.background);
        
        float firstButtonY = buttons.get(0).get(0).y;
        
        textFont(kalamBold96);
        textSize(titleTextSize);
        float titleHeight = (textAscent() + textDescent()) * 2f + g.textLeading;
        if(titleHeight > firstButtonY-titlePaddingY*2f) {
            textSize((firstButtonY-titlePaddingY*2f) / titleHeight * g.textSize);
        }
        if(textWidth(titleText) > width-titlePaddingX*2f) {
            textSize((width-titlePaddingX*2f) / textWidth(titleText) * g.textSize);
        }
        textAlign(CENTER, CENTER);
        fill(#ffffff);
        text(titleText, width/2f, firstButtonY/2f);
        
        this.drawButtons();
    }
}
