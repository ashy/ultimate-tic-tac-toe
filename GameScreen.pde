import java.util.Random;

public class GameScreen extends Screen {
    public final float hoverAlpha = 128f; // For selecting spaces while playing
    public final float hoverRadius = 6f;
    public final float hoverInset = 2f;
    public final float bigHoverAlpha = 64f; // Selecting boards
    public final float previewAlpha = 64f; // "Preview" of where you're sending the other player
    public final float previewRadius = 6f;
    public final float previewInset = 2f;
    public final float bigPreviewAlpha = 24f; // Sending player to already won subboard
    public final float highlightAlpha = 128f; // Highlight currently played subboard
    public final float highlightRadius = 6f;
    public final float highlightInset = 2f;
    public final float winAlpha = 64f; // Darken already won subboards
    public final float winRadius = 6f;
    public final float winInset = 2f;
    
    public final float statusBarHeight = 64f;
    public final float statusTextSize = 48f;
    public final float statusTextPadding = 16f;
    
    public final float boardPadding = 0.01f;
    public final float subboardPadding = 0.005f;
    
    public final Player draw = new Player('-', Colors.gray);
    
    public PauseButton pauseButton;
    public boolean paused;
 
    public Player[] players;
    public Player turn;
    public Player winner;
 
    public Subboard[][] board;
    public Subboard curr;
   
    public GameScreen() {
        this.players = new Player[] {
            new Player('X', Colors.blue),
            new Player('O', Colors.orange)
        };
        this.turn = players[0];
        this.winner = null;
        
        this.board = new Subboard[3][3];
        for(int x = 0; x < 3; x++) {
            for(int y = 0; y < 3; y++) {
                this.board[x][y] = new Subboard();
            }
        }
        this.curr = board[1][1];
        
        this.pauseButton = new PauseButton(() -> {
            this.paused = true;
            switchScreen(new PauseScreen(this));
        }, 8f, 8f, 48f);
    }
   
    @Override
    public void setup() {
        this.paused = false;
    }
    
    @Override
    public void draw() {
        background(Colors.background);
        
        this.drawStatusBar();
        
        // Boards
        float availableWidth = width;
        float availableHeight = height-statusBarHeight;
        float offsetX = 0f;
        float offsetY = statusBarHeight;
        
        float boardX, boardY; // board drawn pos
        float subboardX, subboardY;
        float boardSize;
        
        if(availableHeight > availableWidth) { // Portrait
            boardSize = min(availableWidth, availableHeight/2f);
            boardX = offsetX + (availableWidth-boardSize)/2f + boardSize*boardPadding;
            boardY = offsetY + availableHeight/2f-(availableHeight/2f-boardSize)/3f-boardSize + boardSize*boardPadding;
            subboardX = offsetX + (availableWidth-boardSize)/2f + boardSize*boardPadding;
            subboardY = offsetY + availableHeight/2f+(availableHeight/2f-boardSize)/3f + boardSize*boardPadding;
        } else { // Landscape
            boardSize = min(availableHeight, availableWidth/2f);
            boardX = offsetX + availableWidth/2f-(availableWidth/2f-boardSize)/3f-boardSize + boardSize*boardPadding;
            boardY = offsetY + (availableHeight-boardSize)/2f + boardSize*boardPadding;
            subboardX = offsetX + availableWidth/2f+(availableWidth/2f-boardSize)/3f + boardSize*boardPadding;
            subboardY = offsetY + (availableHeight-boardSize)/2f + boardSize*boardPadding;
        }
        boardSize -= boardSize*boardPadding*2;
        
        drawBoard(boardX, boardY, boardSize);
        curr.draw(subboardX, subboardY, boardSize);
        
        // Player needs to choose board to go in
        if(!paused && this.winner == null && (debug || curr.winner != null) && mouseX > boardX && mouseX < boardX+boardSize && mouseY > boardY && mouseY < boardY+boardSize) {
            int bX = (int)((mouseX-boardX)/boardSize*3f);
            int bY = (int)((mouseY-boardY)/boardSize*3f);
            
            float subboardSize = boardSize/3f;
            
            if(this.board[bX][bY].winner == null) { // Hover colors
                fill(Colors.white, hoverAlpha);
            } else {
                fill(Colors.red, hoverAlpha);
            }
            noStroke();
            rect(boardX + bX*subboardSize + hoverInset, boardY + bY*subboardSize + hoverInset, subboardSize - hoverInset*2, subboardSize - hoverInset*2, hoverRadius);
            
            if(this.board[bX][bY].winner == null && !mousePressed && lastMousePressed) {
                this.curr = this.board[bX][bY];
            }
        }
        // Red if subboard hovered when player needs to choose
        if(!paused && this.winner == null && curr.winner != null && mouseX > subboardX && mouseX < subboardX+boardSize && mouseY > subboardY && mouseY < subboardY+boardSize) {
            fill(Colors.red, bigHoverAlpha);
            noStroke();
            rect(subboardX+hoverInset, subboardY+hoverInset, boardSize-hoverInset*2, boardSize-hoverInset*2, hoverRadius);
        }
        
        // Subboard buttons
        if(!paused && this.winner == null && curr.winner == null && mouseX > subboardX && mouseX < subboardX+boardSize && mouseY > subboardY && mouseY < subboardY+boardSize) {
            int sbX = (int)((mouseX-subboardX)/boardSize*3f);
            int sbY = (int)((mouseY-subboardY)/boardSize*3f);
            
            float spaceSize = boardSize/3f;
            
            if(curr.getSpace(sbX, sbY) == null) { // Hover colors
                fill(turn.col, hoverAlpha);
            } else {
                fill(Colors.red, hoverAlpha);
            }
            noStroke();
            rect(subboardX + sbX*spaceSize + hoverInset, subboardY + sbY*spaceSize + hoverInset, spaceSize - hoverInset*2, spaceSize - hoverInset*2, hoverRadius);
            
            if(curr.getSpace(sbX, sbY) == null) { // Preview text
                textFont(kalamBold255);
                textSize(spaceSize);
                textAlign(CENTER, CENTER);
                fill(turn.col);
                text(turn.symbol, subboardX + sbX*spaceSize + 0.5f*spaceSize, subboardY + sbY*spaceSize + 0.5f*spaceSize);
            }
            
            if(curr.getSpace(sbX, sbY) == null) { // Space empty
                if(board[sbX][sbY].winner != null) { // Preview on big board
                    fill(Colors.white, bigPreviewAlpha);
                    noStroke();
                    rect(boardX+hoverInset, boardY+hoverInset, boardSize-hoverInset*2, boardSize-hoverInset*2, hoverRadius);
                } else {
                    fill(Colors.white, previewAlpha);
                    noStroke();
                    rect(boardX + sbX*spaceSize + previewInset, boardY + sbY*spaceSize + previewInset, spaceSize - previewInset*2, spaceSize - previewInset*2, previewRadius);
                }
                
                if(!mousePressed && lastMousePressed) { // Clicked
                    if(this.curr.setSpace(sbX, sbY, turn) != null) { // Winning move
                        this.checkWinner();
                    }
                    this.curr = board[sbX][sbY];
                    
                    this.swapTurn();
                }
            }
        }
        
        // Win popup
        if(!paused && winner != null) {
            float rectHeight = 128f;
            
            fill(Colors.black, 192f);
            noStroke();
            rect(12f, height/2f-rectHeight/2f, width-24f, rectHeight, 24f);
            
            textFont(kalamBold96);
            textSize(96f);
            textAlign(CENTER, CENTER);
            fill(this.winner.col);
            text(this.winner.equals(draw) ? "Draw wins?" : this.winner.symbol + " wins!", width/2f, height/2f);
        }
    }
    
    public void drawStatusBar() {
        float availableWidth = width - (pauseButton.x + pauseButton.w);
        float offsetX = pauseButton.x + pauseButton.w;
        
        String statusText = turn.symbol + "'s turn";
        color statusColor = turn.col;
        
        if(this.curr.winner != null) {
            statusText = turn.symbol + "'s turn. Choose which board to go in";
        }
        if(this.winner != null) {
            statusText = this.winner.symbol + " wins!";
            statusColor = this.winner.col;
        }
        if(this.winner == draw) {
            statusText = "Draw wins?";
            statusColor = this.winner.col;
        }
        
        fill(Colors.darkBlue);
        noStroke();
        rect(0, 0, width, statusBarHeight);
        
        textFont(kalamBold48);
        textSize(statusTextSize);
        if(textWidth(statusText) > availableWidth-statusTextPadding*2f) {
            textSize((availableWidth-statusTextPadding*2f) / textWidth(statusText) * statusTextSize);
        }
        fill(statusColor);
        if(width / 2f - textWidth(statusText)/2f > offsetX) {
            textAlign(CENTER, CENTER);
            text(statusText, width/2f, statusBarHeight/2f);
        } else {
            textAlign(CENTER, CENTER);
            text(statusText, offsetX + availableWidth/2f, statusBarHeight/2f);
        }
        
        this.pauseButton.draw();
    }
    
    @Override
    public void keyPressed() {
        if(keyPressed && !lastKeyPressed) {
            if(key == ESC) {
                this.paused = true;
                switchScreen(new PauseScreen(this));
            }
        }
        if(debug) {
            if(keyPressed && !lastKeyPressed) {
                switch(key) {
                    case 'q': this.curr = board[0][0]; break;
                    case 'w': this.curr = board[1][0]; break;
                    case 'e': this.curr = board[2][0]; break;
                    case 'a': this.curr = board[0][1]; break;
                    case 's': this.curr = board[1][1]; break;
                    case 'd': this.curr = board[2][1]; break;
                    case 'z': this.curr = board[0][2]; break;
                    case 'x': this.curr = board[1][2]; break;
                    case 'c': this.curr = board[2][2]; break;
                    case TAB: this.swapTurn(); break;
                }
            }
        }
    }
    
    public void drawBoard(float x, float y, float size) {
        for(int boardX = 0; boardX < 3; boardX++) {
            for(int boardY = 0; boardY < 3; boardY++) {
                float subboardSize = size/3f;
                
                float startX = x + boardX * subboardSize;
                float startY = y + boardY * subboardSize;
                
                board[boardX][boardY].draw(startX+subboardPadding*size, startY+subboardPadding*size, subboardSize-subboardPadding*size*2f);
                
                if(this.winner == null && board[boardX][boardY] == curr && curr.winner == null) { // Highlight current subboard
                    fill(Colors.white, highlightAlpha);
                    noStroke();
                    rect(startX+highlightInset, startY+highlightInset, subboardSize-highlightInset*2f, subboardSize-highlightInset*2f, highlightRadius);
                }
            }
        }
    }
    
    public Player checkWinner() {
        // Columns
        for(int x = 0; x < 3; x++) {
            Player p = this.board[x][0].winner;
            if(p == null || p.equals(draw)) {
                continue;
            }
            if(p.equals(board[x][1].winner) && p.equals(board[x][2].winner)) {
                this.winner = p;
                return p;
            }
        }
        // Rows
        for(int y = 0; y < 3; y++) {
            Player p = this.board[0][y].winner;
            if(p == null || p.equals(draw)) {
                continue;
            }
            if(p.equals(board[1][y].winner) && p.equals(board[2][y].winner)) {
                this.winner = p;
                return p;
            }
        }
        
        // Diagonals
        if(this.board[0][0].winner != null && !this.board[0][0].winner.equals(draw)) {
            Player p = this.board[0][0].winner;
            if(p.equals(board[1][1].winner) && p.equals(board[2][2].winner)) {
                this.winner = p;
                return p;
            }
        }
        if(this.board[2][0].winner != null && !this.board[2][0].winner.equals(draw)) {
            Player p = this.board[2][0].winner;
            if(p.equals(board[1][1].winner) && p.equals(board[0][2].winner)) {
                this.winner = p;
                return p;
            }
        }
            
        for(int x = 0; x < 3; x++) {
            for(int y = 0; y < 3; y++) {
                if(this.board[x][y].winner == null) {
                    this.winner = null;
                    return null;
                }
            }
        }
        
        this.winner = draw;
        return draw;
    }
    
    public void swapTurn() {
        if(this.turn.equals(this.players[0])) {
            this.turn = this.players[1];
        } else {
            this.turn = this.players[0];
        }
    }
    
    public class Player {
        public char symbol;
        public color col;
        
        public Player(char symbol, color col) {
            this.symbol = symbol;
            this.col = col;
        }
        public boolean equals(Object obj) {
            if(this == obj) {
                return true;
            }
            if(obj == null) {
                return false;
            }
            if(this.getClass() != obj.getClass()) {
                return false;
            }
            return this.hashCode() == obj.hashCode();
        }
        public int hashCode() {
            return ((int)symbol << 24) | (col & 0x00ffffff);
        }
    }

    public class Subboard {
        public static final float lineWeight = 0.02f;
        public static final float lineRandomness = 0.05f;
        
        public long seed;
    
        public Player[][] board;
        public Player winner;
        
        public Subboard(Player[][] board) {
            Random random = new Random();
            this.seed = random.nextLong();
            
            this.board = board;
            this.winner = null;
        }
        public Subboard() {
            this(null);
            board = new Player[3][3];
            /*
            for(int x = 0; x < 3; x++) {
                for(int y = 0; y < 3; y++) {
                    if(Math.random() > 0.5)
                        board[x][y] = Math.random() > 0.5 ? players[0] : players[1];
                }
            }*/
        }
        
        public Player setSpace(int x, int y, Player to) {
            this.board[x][y] = to;
            return checkWinner();
        }
        public Player getSpace(int x, int y) {
            return this.board[x][y];
        }
        
        public Player checkWinner() {
            // Columns
            for(int x = 0; x < 3; x++) {
                Player p = this.board[x][0];
                if(p == null) {
                    continue;
                }
                if(p.equals(board[x][1]) && p.equals(board[x][2])) {
                    this.winner = p;
                    return p;
                }
            }
            // Rows
            for(int y = 0; y < 3; y++) {
                Player p = this.board[0][y];
                if(p == null) {
                    continue;
                }
                if(p.equals(board[1][y]) && p.equals(board[2][y])) {
                    this.winner = p;
                    return p;
                }
            }
            
            // Diagonals
            if(this.board[0][0] != null) {
                Player p = this.board[0][0];
                if(p.equals(board[1][1]) && p.equals(board[2][2])) {
                    this.winner = p;
                    return p;
                }
            }
            if(this.board[2][0] != null) {
                Player p = this.board[2][0];
                if(p.equals(board[1][1]) && p.equals(board[0][2])) {
                    this.winner = p;
                    return p;
                }
            }
            
            for(int x = 0; x < 3; x++) {
                for(int y = 0; y < 3; y++) {
                    if(this.board[x][y] == null) {
                        this.winner = null;
                        return null;
                    }
                }
            }
            
            this.winner = draw;
            return draw;
        }
        
        public void draw(float x, float y, float size) {
            float lineInset = (size-lineWeight*2f)/3f + lineWeight/2f; // Just doing size/3 would make uneven squares
            float startX = x;
            float startY = y;
            float endX = x + size;
            float endY = y + size;
            
            Random random = new Random(seed);
            
            stroke(#ffffff);
            strokeWeight(lineWeight*size);
            strokeCap(ROUND);
            
            line(startX + lineInset + (random.nextFloat()*lineRandomness*size)-lineRandomness*size/2f, startY + (random.nextFloat()*lineRandomness*size/2f),
                 startX + lineInset + (random.nextFloat()*lineRandomness*size)-lineRandomness*size/2f, endY - (random.nextFloat()*lineRandomness*size/2f)
            );
            line(endX - lineInset + (random.nextFloat()*lineRandomness*size)-lineRandomness*size/2f, startY + (random.nextFloat()*lineRandomness*size/2f),
                 endX - lineInset + (random.nextFloat()*lineRandomness*size)-lineRandomness*size/2f, endY - (random.nextFloat()*lineRandomness*size/2f)
            );
            
            line(startX + (random.nextFloat()*lineRandomness*size/2f), startY + lineInset + (random.nextFloat()*lineRandomness*size)-lineRandomness*size/2f,
                 endX - (random.nextFloat()*lineRandomness*size/2f),   startY + lineInset + (random.nextFloat()*lineRandomness*size)-lineRandomness*size/2f
            );
            line(startX + (random.nextFloat()*lineRandomness*size/2f), endY - lineInset + (random.nextFloat()*lineRandomness*size)-lineRandomness*size/2f,
                 endX - (random.nextFloat()*lineRandomness*size/2f),   endY - lineInset + (random.nextFloat()*lineRandomness*size)-lineRandomness*size/2f
            );
            
            float spaceSize = size/3f;
            
            textFont(kalamBold255);
            textSize(spaceSize-lineWeight);
            textAlign(CENTER, CENTER);
            
            for(int boardX = 0; boardX < 3; boardX++) {
                for(int boardY = 0; boardY < 3; boardY++) {
                    if(board[boardX][boardY] == null) {
                        continue;
                    }
                    
                    fill(board[boardX][boardY].col);
                    text(board[boardX][boardY].symbol, startX + spaceSize*boardX + spaceSize/2f, startY + spaceSize*boardY + spaceSize/2f);
                }
            }
            
            if(this.winner != null) {
                if(this.winner.equals(GameScreen.this.winner)) {
                    fill(this.winner.col, winAlpha);
                } else {
                    fill(Colors.black, winAlpha);
                }
                noStroke();
                rect(startX+winInset, startY+winInset, size-winInset*2f, size-winInset*2f, winRadius);
                
                textSize(size-winInset*2f);
                fill(winner.col);
                text(winner.symbol, startX+size/2f, startY+size/2f);
            }
        }
    }
    
    public class PauseButton extends MenuButton {
        public float iconStrokeWeight = 8f;
        
        public float size;
        
        public PauseButton(Runnable onClick, float x, float y, float size) {
            super(onClick, "=", x, y, size, size);
            
            this.fillColor = Colors.lessDarkBlue;
            this.textColor = Colors.white;
            this.strokeColor = Colors.white;
            this.strokeWeight = 0.0f;

            this.hoverFillColor = Colors.lessDarkBlue;
            this.hoverTextColor = Colors.white;
            this.hoverStrokeColor = Colors.white;
            this.hoverStrokeWeight = 6.0f;
            
            this.font = kalamBold48;
            this.btnTextSize = 48f;
            this.radius = 6f;
            
            this.size = size;
        }
        
        @Override
        public void draw() {
            super.draw();
        }
        
        @Override
        public boolean isHovered() {
            return !GameScreen.this.paused && super.isHovered();
        }
    }
}
