public class Colors {
    public static final color background = #224466;
    public static final color darkBlue = #18324d;
    public static final color lessDarkBlue = #204266;
    public static final color blue = #88ddff;
    public static final color orange = #ffd988;
    public static final color red = #ff0000;
    public static final color white = #ffffff;
    public static final color gray = #b0b0b0;
    public static final color black = #000000;
}
