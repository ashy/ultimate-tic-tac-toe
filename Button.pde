public class Button {
    public float x, y;
    public float w, h;

    public Runnable onClick;

    public void draw() {
        if(this.isClicked()) {
            this.onClick.run();
        }
    }
    
    public boolean isHovered() {
        return mouseX > x && mouseX < x+w && mouseY > y && mouseY < y+h;
    }
    public boolean isClicked() {
        return this.isHovered() && !mousePressed && lastMousePressed;
    }
}
