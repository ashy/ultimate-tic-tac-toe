public abstract class Screen {
    public abstract void setup();
    public abstract void draw();
    
    public void keyPressed() {
        
    }
}

Screen currentScreen;

void switchScreen(Screen to) {
    currentScreen = to;
    to.setup();
}
