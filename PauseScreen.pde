public class PauseScreen extends MenuScreen {
    public final String titleText = "Paused";
    public final float titleTextSize = 96f;
    public final float titlePaddingX = 16f;
    public final float titlePaddingY = 0f;
    
    public GameScreen game;
    
    public PauseScreen(GameScreen game) {
        super();
        this.game = game;
    }
    
    @Override
    public void setup() {
        super.setup();
        this.buttons.add(List.of(new MenuButton[] {
            new MenuButton(() -> {
                switchScreen(game);
            }, "Back to Game")
        }));
        this.buttons.add(List.of(new MenuButton[] {
            new MenuButton(() -> {
                switchScreen(new OptionsScreen());
            }, "Options")
        }));
        this.buttons.add(List.of(new MenuButton[] {
            new MenuButton(() -> {
                switchScreen(new TitleScreen());
            }, "Quit to Title")
        }));
    }
    @Override
    public void draw() {
        this.yOffset = height/3f - (btnHeight*buttons.size() + btnPadding*(buttons.size()-1))/2f;
        super.draw();
        game.draw();
        
        // Darken background
        fill(Colors.background, 192);
        noStroke();
        rect(0,0, width,height);
        
        // Title
        float firstButtonY = buttons.get(0).get(0).y;
        
        textFont(kalamBold96);
        textSize(titleTextSize);
        float titleHeight = (textAscent() + textDescent()) + g.textLeading;
        if(titleHeight > firstButtonY-titlePaddingY) {
            textSize((firstButtonY-titlePaddingY) / titleHeight * g.textSize);
        }
        if(textWidth(titleText) > width-titlePaddingX*2f) {
            textSize((width-titlePaddingX*2f) / textWidth(titleText) * g.textSize);
        }
        textAlign(CENTER, CENTER);
        fill(#ffffff);
        text(titleText, width/2f, firstButtonY/2f);
        
        // Buttons
        this.drawButtons();
    }
    public void keyPressed() {
        if(keyPressed && !lastKeyPressed) {
            if(key == ESC) {
                switchScreen(game);
            }
        }
    }
}
