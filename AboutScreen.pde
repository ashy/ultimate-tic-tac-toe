public class AboutScreen extends Screen {
    private AboutButton button; 
    
    @Override
    public void setup() {
        this.button = new AboutButton(() -> {
            switchScreen(new TitleScreen());
        });
    }
    @Override
    public void draw() {
        background(Colors.background);
        
        textFont(kalamBold48);
        textSize(48f);
        textAlign(CENTER, CENTER);
        fill(#ffffff);
        text("This game is cool", 0, 0, width, height);
        
        this.button.draw();
    }
    
    public class AboutButton extends Button {
        public AboutButton(Runnable onClick) {
            super();
            this.x = 0;
            this.y = 0;
            this.w = width;
            this.h = height;
            this.onClick = onClick;
        }
        
        @Override
        public void draw() {
            super.draw();
            this.w = width;
            this.h = height;
        }
    }
}
